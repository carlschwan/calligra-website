---
title: Calligra Plan version 3.2.2 released
date: 2019-12-16
author: danders
categories: [announcements,stable]
---

We are pleased to announce the relase of Calligra Plan 3.2.2.

Tarball can be found here:

[http://download.kde.org/stable/calligra/3.2.2/calligraplan-3.2.2.tar.xz.mirrorlist](http://download.kde.org/stable/calligra/3.2.2/calligraplan-3.2.2.tar.xz.mirrorlist)

**The following bugs have been fixed:**

```
* Bug 415041 - Default calendar not set when creating new project
* Bug 414783 - Remaining effort not merged if nothing else has changed
* Bug 414781 - Stray () paranteses displayed in schedule selector
* Bug 414661 - Can't add progress finish time in the past


```
