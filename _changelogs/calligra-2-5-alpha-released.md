---
title: Calligra 2.5 Alpha Released
date: 2012-05-30
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce the alpha release of version 2.5 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). Since the first release of Calligra the team has worked using a new development model where new features are only introduced into the main sources (known as "master") after a feature or bugfix is "finished" in some sense. The actual development is done in a branch which is kept separate from the main sources while the code is still immature.

This allows us to use shorter development cycles and shorter alpha and beta periods since the master branch should in principle be releasable at any time. However, all software contains bugs and in 3 weeks we will announce the 2.5beta version. This also marks the hard feature freeze for version 2.5. After this there will only be bugfixes on the release branch until the final release in July.

\[caption id="attachment\_3379" align="aligncenter" width="500" caption="Tight run-around in Words"\][![](/assets/img/our-planet-earth_screenshot-500x285.png "Tight run-around in Words")](http://www.calligra.org/news/calligra-2-5-alpha-released/attachment/our-planet-earth_screenshot/)\[/caption\]

## News in This Release

As usual the release has a number of new or improved features. The most important of them will be highlighted here. There are still a few important improvements that we expect for the 2.5 beta and final versions but which were not ready in time for this alpha.

### Productivity Applications

For the productivity part of the suite (word processor, spreadsheet, and presentation program) the target user of version 2.5 is still the student or academic user. This version has a number of new features that will make it more suitable for these users.

**Words**, the word processor, has [among other things](http://community.kde.org/Calligra/Schedules/2.5/Feature_Plan#Words) improved support for [editing of tables](http://player.vimeo.com/video/40126803), tight run-around of text around images, manipulation of table borders, and dragging of text. There are also a number of new features in the bibliography tools.

**Stage**, the presentation program, has a number of usability improvements.

**Flow**, the diagram applicatio,n has support for new stencils in odf custom shapes.

**Braindump**, the note taking application, has no new features in itself but will take advantage of all the improvements in shared shapes and plugins.

### Artistic Applications

The artistic applications of the Calligra Suite are the most mature ones and are already used by professional users everywhere.

**Krita**, the drawing application, has a [new compositions docker](http://slangkamp.wordpress.com/2012/05/09/krita-compositions-docker/), useful in movie storyboard generation. Other new features are textured painting, improved canvas rotation,  management of autosave files and many preformance improvements.

## Filters

There are three new filters in Calligra 2.5alpha:

- A new import filter for **Visio** files.
- A new import filter for **MS Works**.
- A new import filter for **xfig** files.

And of course all the filters for **MS Office** have been improved even more, especially for the OOXML formats used in MS Office 2007 and 2010.

## Common Improvements

The architecture of the Calligra Suite lets the applications share much of the functionality of the suite with each other. Many common parts have seen improvements since the release of 2.4. Here are a few of them.

**Images have new support for various filters** such as gray and black/white image effects. There is also new support for mirroring images on any axis.

**Charts** have a number of improvements and work much better in general.

The common parts of Calligra was the subject of a week-long bug fixing sprint where over 100 bugs was closed. This means that the overall quality of the shared plugins is improved.

## Other Than the Desktop

At the same time as the desktop version, the community also releases a QML based version for tablets and smartphone: **Calligra Active**. The Calligra team has decided to focus on Calligra Active as the main mobile version for the future.

## Try It Out

The source code of the snapshot is available for download: [calligra-2.4.90.tar.bz2](http://download.kde.org/unstable/calligra-2.4.90/calligra-2.4.90.tar.bz2)

### Ubuntu

Users of Ubuntu and Kubuntu are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:

sudo add-apt-repository ppa:neon/ppa \\
&& sudo apt-get update\\
&& sudo apt-get install project-neon-base \\
   project-neon-calligra \\
   project-neon-calligra-dbg

You can run these packages by adding /opt/project-neon/bin to your PATH.

### Arch Linux

Arch Linux provides Calligra packages in the \[kde-unstable\] repository.

### Fedora

Fedora packages are available in the rawhide development repo ([http://fedoraproject.org/wiki/Rawhide](http://fedoraproject.org/wiki/Rawhide)), and unofficial builds are available for prior releases from kde-unstable repo at [http://kde-redhat.sourceforge.net/](http://kde-redhat.sourceforge.net/) .

### OpenSuse

There are OpenSUSE Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).

### FreeBSD

Calligra ports are available in [Area51](http://freebsd.kde.org/area51.php).

### Windows and OSX

Windows packages will be available from [KO GmbH](http://www.kogmbh.com/). For download of Windows binaries, use the [download page](http://www.kogmbh.com/download.html). We would welcome volunteers who want to build and publish packages for the Calligra Suite on OSX.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
