---
title: Calligra 2.9.4 Released
date: 2015-05-06
author: jstaniek
categories: []
---

One week ago the Calligra team has noticed issues with handling of Photoshop (PSD) files in Krita, and decided to [postpone](/news/2-9-3-cancelled/) the 2.9.3 release. Today the issue is fixed and a new version, 2.9.4, is ready to install for the [Calligra Suite](http://www.calligra.org "Home page of the Calligra Suite"), Calligra Active and the Calligra Office Engine. We recommend updating the software to everybody.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-4-released/#support)

## Bugfixes in This Release

Here is an overview of the most important fixes. There are several others that may be not mentioned here.

### General

- Text Shape: fix the styles combo box after adding a new style

### Kexi

- _General_:
    - Close window with object that will be overwritten after "Save as" (bug 344656)
    - Remove "dirty" flag when saving-as modified Kexi object (bug 344653)
    - Save recent position and size of the Kexi Find dialog. It has now minimal size to avoid covering too much of the content. (bug 345689)
    - Properly destroy Kexi's main window on closing. This lets to properly save settings and clean up.
    - Activate Design tab on switching to design mode (if no other tab was explicitly selected for this window & mode) (bug 335900)
- _Query Designer_:
    - Allow to switch from the Design view containing empty query to the SQL view (bug 344654)
    - Show "Incorrect query" message when switching to Data view from Kexi SQL containing an empty statement. Before unclear "Switching view failed" message was presented.
    - Avoid crash in Kexi text editor when text component couldn't be loaded (applies to script editors too) (bug 346373)
- _Forms_:
    - Fix computing type for widget properties. If the custom type is invalid or auto (unspecified), use what Qt properties define. If still undefined, default is the String type, not a pixmap. This fixes at least the map widget's properties latitude/longitude that were handled as pixmaps.
    - Fix logic for adding new records for Kexi tables and forms. (bug 345210)
    - Map widget: set double type for latitude/longitude properties, and set precision to 7 digits
    - Map widget: improve translation of latitude, longitude, zoom and theme property names
    - Map widget: theme always defaults to Earth and is now a list of all available themes
    - Map widget: property editor's spin box is now accessible as a slider
    - Map widget: don't start editing in response to resize events
- _Report_:
    - Map element: properly encode unit name (°)
    - Map element: set double type for latitude/longitude properties, and set precision to 7 digits
    - Map element: improve translation of latitude, longitude, zoom and theme property names
    - Map element: theme always defaults to Earth and is now a list of all available themes
    - Map element: property editor's spin box is now accessible as a slider
- _CSV Import_:
    - Usability improvements:
        - Enable single selection of table cells ("current" cell indicator is not visible in Breeze style)
        - Display font of edited column names in bold also during editing
        - Allow editing only first row (column names) but also when "First row contains column names" is off; this let's the user to fully customize column names before importing
        - Use "Column <n>" column caption and "column\_<n>" column name for empty names

### Krita

- Implement Photoshop-style layer styles. Note: this is the first version. Some features are not implemented and we load and save only to Krita's native file format and ASL style library files. PSD files are not supported yet.
- Fix issues with the default settings button
- Fix the patchcount of the color history
- Lots of fixes to the layout of docker panels, dialogs and other parts of Krita
- Lots of fixes for special widgets when using the Plastique style
- Fix issues with resizing the icon size in resource selectors
- Fix usability issues in the crop tool
- Add a function to hide docker titlebars
- Save memory by not loading or saving texture information for brush presets that don't use textures
- Automatically add a tag based on the filename for all brush tips from Adobe ABR brush collections
- Make Export and Save as default to the folder the original file came from
- Make it possible to switch off compression for layers in .kra files (bigger files, but faster saving)
- Disable opening 32 bit float gray scale TIFF files: we don't support that yet
- Fix memory leak when using gradients
- Fix color serialization from ui to gmic (bug 345639)
- Fix crash when toggling gmic preview check box (bug 344058)
- Make it possible to re-enable the splash screen
- Make startup faster by not waiting for the presets to be loaded
- Show the label for the sliders inside the slide, to save space.
- Fix HSV options for the grid and spray brush
- Don't show the zoom on-canvas notification while loading an image
- Fix many memory leaks
- Big startup speed improvement
- Big speed improvement when using transform masks
- Fix the specific color selector so it doesn't grow too big
- Allow the breeze theme to be used on platforms other than KDE Plasma
- Don't crash when creating a pattern-filled shape if no pattern is selected (bug 346990)
- Fix loading floating point TIFF files (bug 344334)
- Fix loading tags for resources from installed bundles
- Make it possible to ship default tags for our default resources (bug 338134 -- needs more work to create a good default definition)
- Remember the last chosen tag in the resource selectors (bug 346703)
- Don't say "All presets" in the brush tip selector's filter (bug 346355)

### Karbon

- Show color palettes again in the bottom bar

### Calligra Stage

- Fix crash after empty custom slide show.
- Fix showing of thumbnail after updating master page (also fixes Calligra Flow)
- Fix showing thumbnail on page content change (also fixes Calligra Flow)

### Calligra Sheets

- Fix CSV import (bug 344718)

  

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.9.4.tar.xz](http://download.kde.org/stable/calligra-2.9.4/calligra-2.9.4.tar.xz). Also [translations to many languages](http://download.kde.org/stable/calligra-2.9.4/calligra-l10n/) and [MD5 sums](http://download.kde.org/stable/calligra-2.9.4/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release).

  

## What's Next and How to Help?

The next step after the [2.9 series](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) is Calligra 3.0 which will be based on new technologies. We expect it later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated to [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita). Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, travelling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to: ![Heart](/assets/img/heart-701.png)

**Support entire Calligra indirectly by donating to KDE**, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations).

![Heart](/assets/img/heart-701.png)

**Support Krita directly by donating to the Krita Foundation**, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/).

![Heart](/assets/img/heart-701.png)

**Support Kexi directly by donating to its current BountySource fundraiser**, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

Calligra Suite is a graphic art and office suite developed by the KDE community. It is available for desktop PCs, tablet computers and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics and digital painting. For more information visit [calligra.org](http://www.calligra.org/).

  

## About KDE

![](/assets/img/klogo-official-lineart_simple_bw-128x128.png)

[KDE is an international technology team](https://www.kde.org/community/whatiskde/) that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet, multimedia, entertainment, education, graphics and software development. KDE's software available in more than 60 languages on Linux, BSD, Solaris, Windows and Mac OS X.

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
