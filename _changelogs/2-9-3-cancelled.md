---
title: Cancelled Release of Calligra 2.9.3
date: 2015-04-29
author: jstaniek
categories: []
---

The Calligra team has noticed some issues with handling of Photoshop (PSD) files in Krita. We understand this as an important missing feature so the release 2.9.3 has been cancelled and supplementary 2.9.4 release is expected in a week. We're sorry for the inconvenience.

It may happen that distributors deliver 2.9.3 for your operating system. If this is the case and you do not depend on the PSD feature of Krita, you can safely switch to the 2.9.3 version. In other case we recommend not upgrading and staying with the version 2.9.2 or older until the 2.9.4 arrives.
