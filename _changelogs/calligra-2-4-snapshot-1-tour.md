---
title: Calligra 2.4 Snapshot 1 Tour
date: 2011-05-18
author: stephdg
categories: []
---

# **Introduction To The Calligra Suite**

The Calligra Suite is a set of applications that allows you to easily complete your work. There are office applications, as well as Graphic applications. There is also a comprehensive set of plug-ins. The Calligra Suite is unique because not only does it consist of the normal word processor (Words) and spreadsheet (Tables) applications, but it also brings you Graphic applications as well.

The Calligra Suite consists of 8 applications, the office applications are Words, Tables, Stage, Kexi, Flow, and Plan, and the graphic applications are Krita and Karbon. Each application is unique and aimed towards normal personal and professional uses.

The Calligra Suite runs on all platforms. The Desktop edition runs on Windows, Linux and other Unix variations. Calligra Mobile currently runs using the Maemo 5 OS, and Meego platforms.

### Common Features

Use The Calligra Suite is create to different environments and builds with different feature sets. When setting up The Calligra Suite you have the ability to choose which applications you wish to install. Customise The Calligra Suite installation by using any of the 20 plug-ins from shapes to text fields in any manner you want.

Open Document Format - Uses ISO standardised format that is compatible between many office suites / applications and also one that is decided as national standard in many countries. ODF is the primary setting in The Calligra Suite, however it allows you to open any supported file types, edit them, and then save them as ODF. This allows it to be compatible with numerous other suites such as, OpenOffice.org, LibreOffice, Google Docs, Lotus Symphony, Microsoft Office, and many others.

## Calligra Desktop

_The dockers concept_ : With the flexible user interface, tool bars can be moved and pulled off the main window, and placed in your desired spot. The tool-bars of The Calligra Suite are not locked in position, they are easy to move, and place wherever you desire. They also can be removed and placed as separate windows. This ability makes the layout look much cleaner and is targeted towards wide screens.

_Embeddable Objects_ : There are many shapes that you can insert into your document, such as geometric shapes, sheet music score, and many more amazing options. All of these shapes are available in all applications, which makes implementing these objects much easier.

[![](/assets/img/shapes.png "shapes")](http://www.calligra-suite.org/?attachment_id=1980)

### Office Applications

1\. _Words_ is an word processor application with desktop publishing features. You can create informative and attractive documents with a few clicks of your mouse. This application is simple to use. Adding images and graphs is as easy as dragging them into the document. If you don't like the placement of the image, simply move it around and while you are moving it, the text will automatically wrap around it. Adding charts and spreadsheets is also that simple, and it's easy to re-size and rotate them as well. [](http://www.calligra-suite.org/?attachment_id=1965)[![](/assets/img/words3-500x275.png "words3")](http://www.calligra-suite.org/wp-content/uploads/2011/05/words3.png)[![](/assets/img/words4-500x275.png "words4")](http://www.calligra-suite.org/wp-content/uploads/2011/05/words4.png) 2. _Stage_ is a powerful and easy to use presentation application. Stage comes with some amazing, stylish options for your slide backgrounds. Make your slides using images, videos, animation and more. Making presentations has never been easier! With the plug-in's available in The Calligra Suite, there are more options than ever to add to your slides.

[![](/assets/img/stage-500x279.png "stage")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/stage-2/)

[![](/assets/img/stage1-500x276.png "stage1")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/stage1/)

Stage in Presentation Mode

3\. _Tables_ is a fully-featured spreadsheet application. It can be used to quickly create spreadsheets with formulas and charts, to calculate and organise your data in a neat manner. Tables comes with options for General, Business, and Home and Family options with pre-set spreadsheets. It also comes with an astounding array of pre-set formulas.

[![](/assets/img/tables-500x274.png "tables")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/tables-2/)[![](/assets/img/tables1-500x275.png "tables1")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/tables1/)4\. _Flow_ is used to make diagrams and flowcharts.

Use Flow to make network diagrams, organization charts, flowcharts and much more. Flow also comes with numerous stencils that can be used to make anything you want. There are options for Networking, Renewable Energy, Chemistry, Building sites, and many other options to help you make your diagrams.

[![](/assets/img/flow-500x275.png "flow")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/flow-2/)[![](/assets/img/flow1-500x275.png "flow1")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/flow1/)5\. _Kexi_ is an integrated data management program, used to make databases and inserting and processing data. Forms can be created to provide a custom interface to your data. All database objects – tables, queries and forms – are stored in your database, making it easy to you share data and design.

[![](/assets/img/kexi-500x277.png "kexi")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/kexi-3/)[![](/assets/img/kexi2-500x276.png "kexi2")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/kexi2/)[![](/assets/img/kexi3-500x274.png "kexi3")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/kexi3/)6\. _Plan_ is a project management program, geared towards projects with multiple resources.

Use Plan to moderate your projects adequately. Plan offers different types of task dependencies and timing constraints. The usual use case is to define your tasks, estimate the effort needed to perform each task, allocate resources and then let Plan schedule the tasks according to network and resource availability.

If needed the project can be rescheduled while retaining original schedules for comparison. The ability to reschedule from the current state of the project can typically be used when the execution of the project does not go according to plan, resource availability changes or tasks have to be added to or removed from the project, is allowable with this program.

Task progress can be entered in different detail depending on the need for progress monitoring, from a simple percent completed to the more detailed time used per resource and estimate of remaining effort. These detailed progress reports enable the program to calculate the earned value and performance index calculation.

### [![](/assets/img/plangarden-500x275.png "plangarden")](http://www.calligra-suite.org/wp-content/uploads/2011/05/plangarden.png)[![](/assets/img/plangarden1-500x275.png "plangarden1")](http://www.calligra-suite.org/wp-content/uploads/2011/05/plangarden1.png)[![](/assets/img/plangarden2-500x274.png "plangarden2")](http://www.calligra-suite.org/wp-content/uploads/2011/05/plangarden2.png)

### Graphic Applications

1\. _Krita_ is a sketching and painting program. Krita offers an end–to–end solution for creating digital painting files from scratch by masters. Krita supports concept art, creation of comics and textures for rendering.

[![](/assets/img/krita-500x276.png "krita")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/krita-3/)2\. _Karbon_ is used to make vector graphics. This program is great for users starting to explore the world of vector graphics and for artists wanting to create breathtaking vector art.

[![](/assets/img/karbon-500x275.png "karbon")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/karbon-3/)

## Calligra Mobile

Calligra Mobile is the version of the Calligra Suite that is aimed at mobile users using a tablet or a smartphone. It contains applications for word processing, spreadsheets and presentations combined into one application.

Calligra Mobile shares all the advanced core features with the desktop variant of The Calligra Suite such as compatibility with the OpenDocument Format and Microsoft Office 2000, 2003, XP, 2007 and 2010, and a large set of built-in shapes.

In addition to the standard set of features, Calligra Mobile helps you take advantage of the hardware it is running on which means you can include pictures from the built-in camera directly into your documents! Use your smartphone for presentations and use your touch screen and the Calligra highlighting functions to point out areas of interest in your slides! Use your smartphone for reviewing documents while on the bus, train or airplane!

Calligra Mobile is mainly aimed at being a document viewer but can also be used for proofreading by using the basic editing functions that are included.

[![](/assets/img/calligramobile-500x276.png "calligramobile")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/calligramobile/)

Stage in Calligra Mobile via Desktop

[![](/assets/img/calligramobile1-500x276.png "calligramobile1")](http://www.calligra-suite.org/news/calligra-2-4-snapshot-1-tour/attachment/calligramobile1/)

Tables in Calligra Mobile via Desktop

[![](/assets/img/calligramobile2-500x276.png "calligramobile2")](http://www.calligra-suite.org/wp-content/uploads/2011/05/calligramobile2.png)

Words in Calligra Mobile via Desktop
