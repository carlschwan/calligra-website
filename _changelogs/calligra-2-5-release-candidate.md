---
title: Calligra 2.5 Release Candidate
date: 2012-07-18
author: cyrilleberger
categories: []
---

The Calligra team is proud to announce the release candidate of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"). Compared to the beta, this new release contains many bug fixes. However, a regression was made in this release and will be fixed in the next release (see bug [303696](https://bugs.kde.org/show_bug.cgi?id=303696)).

## Try It Out

The source code of this version is available for download here: [calligra-2.4.92.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.4.92/calligra-2.4.92.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download#Unstable) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html).

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Suite

Calligra is a KDE community project. Learn more at [http://www.calligra.org/](http://www.calligra.org/).
