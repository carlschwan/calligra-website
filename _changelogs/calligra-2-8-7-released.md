---
title: Calligra 2.8.7 Released
date: 2014-12-03
author: jstaniek
categories: []
---

This supplementary release 2.8.7 marks the end of [Calligra Suite, and Calligra Active](http://www.calligra.org) 2.8 series. If you update to 2.8.7 (and you should), you'll receive [over 20 improvements](/news/calligra-2-8-7-released/#fixes), mostly in Kexi and Krita.

[Support Calligra!](/news/calligra-2-8-7-released/#support)

We are also glad to note that this release contains first contributions from Roman Shtemberko to Kexi app. Welcome on board!  

## Issues Fixed in This Release

### General

- Fix endless loop in text rendering.
- Add workaround for loading markers with old and broken path. ([details](https://git.reviewboard.kde.org/r/121214/))

### Kexi - Visual Database Applications Builder

- General:
    - Improve main menu widget for QtCurve style.
    - Display "git" version in application's About box.
    - Fix for Mac OS X build.
    - Fix position of the Calligra logo based on height of the main menu. (bug 340767)
    - Ask for password if needed when Test Connection button is clicked in Kexi. ([details](https://community.kde.org/Kexi/Junior_Jobs/Ask_for_password_when_Test_Connection))
    - Fix checking for file overwriting on file creation. ([details](https://reviewboard.kde.org/r/121174/))
    - Fix a few memory leaks and uninitialized variables.
    - Make the main tabbed toolbar more modern, make it work better with more styles (including KDE's next style Breeze)
- Forms:
    - Hide some form unnecessary widget properties in Property Editor. (bug 339246)
    - Fix handling of Ctrl+PageUp/PageDown shortcuts in forms. (bug 338663)
- Reports:
    - Add possibility of creating Kexi reports elements no matter where mouse was pressed and where it was released. Also avoid setting too small size of elements and drawing them outside of the report area. (bug 334967)
    - Fix bug in report toolbox: first element after separator did not function as on/off.
    - Fix old regression: reintroduce report chart plugin ([link](https://community.kde.org/Kexi/Releases/Kexi_2.8#Known_bugs))
    - Improve UX of Report Designer: better styling of section titles: colors, icons, size; draw them in inactive color when focus is out; switch between sections also when titles are clicked or sections resized.
    - Fix width of ruler in "Detail" section of the Report Designer. (bug 338942)

### Krita - Creative Sketching & Painting App

- Fix saving the composed image in PSD files. The composed image data in Photoshop should be pre-filled with white. (bugs 333300, 333322)
- Read dates in Krita RSS feed also on systems with non-english locale.

### Calligra Plan - Project Management App

- Ensure timely order of time intervals of a calendar day. (bug 339840)
- Set current schedule also to all team members. Also fixes crash on calculating a schedule when a task is assigned to a team. (bug 325319)

### Calligra Words - Word Processor

- Render thumbnails optionally like for print (fixes file thumbnails).

## Try It Out

- The source code is available for download: [calligra-2.8.7.tar.xz](http://download.kde.org/stable/calligra-2.8.7/calligra-2.8.7.tar.xz).
- Calligra binaries are available for [download](http://userbase.kde.org/Calligra/Download) for many operating systems.

## What's Next and How to Help?

We're approaching the era of [2.9](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan) to be released in early 2015. It will be followed by Calligra 3.0 based on new technologies later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita) forums. Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(if you'd like to maintain entire app, [Karbon](https://www.calligra.org/karbon/) and [Plan](https://www.calligra.org/plan/) need new [maintainers](https://community.kde.org/Calligra/Maintainers))

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, traveling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to:

- Support entire Calligra indirectly by donating to KDE, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations),
- Support Krita directly by donating to the Krita Foundation, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/),
- Support Kexi directly by donating to its current BountySource fundraiser, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## About Calligra

Calligra Suite is a graphic art and office suite developed by the [KDE community](https://www.kde.org). It is available for desktop PCs, tablet computers, and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics, and digital painting. See more information at the website [http://www.calligra.org](http://www.calligra.org/).

<!-- .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
