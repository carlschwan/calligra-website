---
title: Calligra 2.6 Released
date: 2013-02-05
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce the release of version 2.6 of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"), Calligra Active and the Calligra Office Engine. This version is the result of thousands of commits which provide new features, polishing of the user interaction and bug fixes.

As usual, Calligra is developing very fast and there are a lot of new features to be announced. This is also the second feature update after the initial release of Calligra in April last year which means that we are approaching some form of maturity. The team has used a significant part of the development time to polish away usability problems and visual glitches.

## News in This Release

Here are the most important new or improved features in Calligra 2.6.

1. [New Application: Calligra Author](#author)
2. [Productivity Applications](#prod)
3. [Artistic Applications](#art)
4. [Supported Document Formats](#formats)
5. [Common Improvements](#common)
6. [Other Than the Desktop](#active)
7. [Try It Out](#try)

### New Application: Calligra Author

![Calligra Author](/assets/img/author-ad-250.png)**Calligra Author** is a new member of the growing Calligra application family. The application was [announced just after the release of Calligra 2.5](http://www.calligra.org/news/calligra-announces-author/ ) with the following description:

> The application will support a writer in the process of creating an eBook from concept to publication. We have two user categories in particular in mind:
> 
> - Novelists who produce long texts with complicated plots involving many characters and scenes but with limited formatting.
> - Textbook authors who want to take advantage of the added possibilities in eBooks compared to paper-based textbooks.

The first version of Calligra Author is very similar to Calligra Words since the new features of Author are also adopted by Words. Future releases shall deviate more.

Features that were developed especially for Author include export to eBook formats EPUB2 and MOBI (the latter will come a bit delayed, in 2.6.1), and improved text statistics (word count, character count, etc).

### Productivity Applications

For the productivity part of the suite (word processor, spreadsheet, and presentation program) the target user of version 2.6 is still the student or academic user. This version has a number of new features that will make it more suitable for these users.

**Words**, the word processing application improved layout and numerous minor fixes in dialogs and tools, e.g. the spell checking. This should make Words more pleasant to use for the normal user. Words also enjoys the same features as Author: improved text statistics and export to eBook formats.

\[caption id="attachment\_3740" align="aligncenter" width="540"\][![Statistics support in Calligra Words 2.6](/assets/img/calligra-words-2.6-statistics_sm1.png)](http://www.calligra.org/?attachment_id=3736) Statistics support in Calligra Words 2.6\[/caption\]

**Sheets**, the spreadsheet application, has a new function optimizer - also known as a Solver. All standard scripts are available for translation.

**Stage**, the presentation program, has a new animation framework that lets the user create and manipulate animations in slide shows.

**Flow**, the diagram application, has improved connections.

**Plan**, the project management application, has updated scheduling information, improved scheduling granularity, improved performance charts, improved project creation, improved usability in the report generator and many bug fixes.

**Kexi**, the visual database creator, has new support for user data storage, improved CSV import and export and improved table views. Overwriting objects with the same name is now possible. Detailed lists of changes for Kexi are available [here](http://community.kde.org/Kexi/Releases/Kexi_2.6).

\[caption id="attachment\_3721" align="aligncenter" width="550"\][![Text trimming in tables - Kexi 2.6](/assets/img/kexi-2.6-text-trimming-in-tables_sm.png)](http://www.calligra.org/news/calligra-2-6-released/attachment/kexi-2-6-text-trimming-in-tables/) Text trimming in tables - Kexi 2.6\[/caption\]

### Artistic Applications

The artistic applications of the Calligra Suite are the most mature ones and are already used by professional users everywhere.

**Krita**, the drawing application, has new support for OpenColorIO, which makes it suitable for use in the movie industry. It also has big speedups in many places, including the [vc library](http://code.compeng.uni-frankfurt.de/projects/vc) and when painting using the new precision slider in the preset editor.

\[caption id="attachment\_3589" align="aligncenter" width="500"\][![Screenshot of Krita 2.6 alpha](/assets/img/krita-lut-2.6-alpha-500x289.png)](http://www.calligra.org/news/calligra-2-6-alpha-released/attachment/krita-lut-2-6-alpha/) Setting Exposure on a HDR image with Krita 2.6's new OpenColorIO based LUT docker\[/caption\]

There is also improvements in painting HDR images and integration into a GFX workflow as well as support for the latest OpenRaster standard and many bug fixes.

A full description of Krita is available as a [PDF booklet](http://krita.org/aboutkrita26.pdf). More [screenshots](http://krita.org/krita_26_screenshots.zip) can also be downloaded.

### Supported Document Formats

Calligra 2.6 can now export documents to **EPUB2** format. In 2.6.1 it will also be able to export to **MOBI** format.

And of course support for all **MS Office** formats has been improved even more, especially for the MS Open XML formats of MS Office 2007 and above.

### Common Improvements

The architecture of the Calligra Suite lets the applications share much of the functionality of the suite with each other. Many common parts have seen improvements since the release of 2.5. Here are a few of them.

Calligra will now load and save **3D shapes and annotations**. This means better interoperability with other office applications even if Calligra can not show them at this time.

**Charts** have a number of improvements for fine tuning the formatting. Examples include fonts for titles and labels and markers for data sets.

**Mathematical formulas** have an improved rendering.

### Other Than the Desktop

**Calligra Active**, the version for tablets and smartphones, has several improvements: there is a new slide chooser for presentations, slides can be changed by flicking the device, page switching improvements for text documents and improved start up sequences including a new splash screen.

There is also a  new preview for text documents, a new text search feature for all 3 document formats, support for translations, a simplified UI and some bug fixes. And, naturally, Calligra Active also benefits from all the improvements in the common parts of Calligra: the libraries and plugins.

### Try It Out

- **The source code** is available for download: [calligra-2.6.0.tar.bz2](http://master.kde.org/stable/calligra-2.6.0/calligra-2.6.0.tar.bz2). As far as we are aware, the following distributions package Calligra 2.6. This information will be updated when we get more details. In addition, many distributions will package and ship Calligra 2.6 as part of their standard set of applications.
- In **Chakra Linux**, Calligra is the default office suite so you don't have to do anything at all to try out Calligra.  Chakra aims to be a [showcase Linux for the "Elegance of the Plasma Desktop"](http://http://www.chakra-project.org/) and other KDE software.
- Users of **Ubuntu and Kubuntu** are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:
    
    sudo add-apt-repository ppa:neon/ppa \\
    && sudo apt-get update\\
    && sudo apt-get install project-neon-base \\
       project-neon-calligra \\
       project-neon-calligra-dbg
    
    You can run these packages by adding /opt/project-neon/bin to your PATH.
- **Arch Linux** provides Calligra packages in the \[kde-unstable\] repository.
- **Fedora** packages are available in the rawhide development repository ([http://fedoraproject.org/wiki/Rawhide](http://fedoraproject.org/wiki/Rawhide)), and unofficial builds are available for prior releases from kde-unstable repository at [http://kde-redhat.sourceforge.net/](http://kde-redhat.sourceforge.net/) .
- There are **OpenSUSE** Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).
- Calligra **FreeBSD** ports are available in [Area51](http://freebsd.kde.org/area51.php).
- **MS Windows** packages will be available from [KO GmbH](http://www.kogmbh.com/). For download of Windows binaries, use the [download page](http://www.kogmbh.com/download.html).
- **Mac OS X:** We would welcome volunteers who want to build and publish packages for the Calligra Suite on OS X.

## About Calligra

Calligra, comprising the Calligra Suite, Calligra Active and the Calligra Office Engine, is part of the applications from the KDE community. See more information at the website [http://www.calligra.org](http://www.calligra.org/).
