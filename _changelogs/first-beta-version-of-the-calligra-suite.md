---
title: First Beta Version of the Calligra Suite
date: 2011-09-14
author: Inge Wallin
categories: []
---

The Calligra team is proud and excited to release the first beta version of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"). Since the start of the Calligra effort, we have had a long and very productive development phase. In the mean time we have released four snapshot versions, also known as alphas, to give the users a chance to see what we are doing.

These alpha versions have been work in progress. This beta marks the beginning of a new phase. The time has come to start polishing the features that we have, and make them production quality.

\[caption id="attachment\_2210" align="aligncenter" width="500" caption="Karbon showing a file from OpenClipart"\][![Karbon showing a file from OpenClipart](/assets/img/karbon_2.4beta-500x356.png "karbon_2.4beta")](http://www.calligra-suite.org/news/first-beta-version-of-the-calligra-suite/attachment/karbon_2-4beta/)\[/caption\]

## News in This Release

As usual the release has a number of new or improved features. The most important of them will be highlighted here.

### Productivity Applications

For the productivity part of the suite (word processor, spreadsheet, and presentation program) the target user of version 2.4 is the student or academic user. This version has a number of new features that will make it more suitable for these users.

**Words**, the word processor has improved support for table of contents, improved support for text tables and new support for citations and bibliographies. There is also a new search bar much like the one found in web browsers that will let the users quickly find text anywhere.

**Tables**, the spreadsheet application, has improved support for large tables and much faster loading.

**Stage**, the presentation program, has a new view for slide sorting. This is a result of a Google Summer of Code project by Paul Mendez. This new slide sorter is also used in the dialog for creating custom presentations. Editing of slide notes has also been much improved.

**Kexi**, the database application, has a new modern menu and startup screen. This change is start of the move to a more modern look, and will be introduced into other parts of Kexi as well.

\[caption id="attachment\_2212" align="aligncenter" width="500" caption="Startup dialog for a new Kexi project"\][![Startup dialog for a new Kexi project](/assets/img/kexi-2.4-startup-new-project-500x251.png "kexi-2.4-startup-new-project")](http://www.calligra-suite.org/news/first-beta-version-of-the-calligra-suite/attachment/kexi-2-4-startup-new-project/)\[/caption\]

**Plan**, the project management application**,** has improved support for printing of many types of charts. There is also improved support for scripting that allows better import of data from external sources.

**Flow**, the diagram application which is a new application in the suite, now has support for downloading new stencils via the KNewStuff system. This makes it possible for users to make their own stencil sets available for other users in an easy way.

**Braindump**, the new application **,** is a tool to dump and organize the content of your brain, such as ideas, drawings, images, texts, etc. This application will be first released in Calligra version 2.4 final.

### Artistic Applications

The artistic applications of the Calligra Suite are the most mature ones and are already used by professional users everywhere.

**Krita**, the drawing application, has a much improved brush editor and management of brush presets. Brushes can now be tagged with semantic markup. There is also a new selection popup palette that makes it very efficient to use while drawing. These improvements are direct results of a well developed dialog between the users and the development team.

\[caption id="attachment\_2196" align="aligncenter" width="500" caption="the new Krita brush editor"\][![Screenshot of the Krita brush editor](/assets/img/brush_editor-500x269.png "brush_editor")](http://www.calligra-suite.org/news/first-beta-version-of-the-calligra-suite/attachment/brush_editor/)\[/caption\]

**Karbon**, the vector drawing application, has improved support for the standard SVG format, making it more suited as an advanced vector graphics editor. There is also a new color bar to let the user select colors from a palette. There are also some very nice improvements to the so called artistic text.

## Common Improvements

The architecture of the Calligra Suite lets the applications share much of the functionality of the suite with each other. Many common parts have seen improvements since the last alpha release. Here are a few of them.

**Images can now be dragged & dropped** into the applications from many different sources, including web browsers. This makes it much easier to create documents containing pictures, especially text documents and presentations.

The whole Calligra Suite has gained support for **images in the SVG format**.

The native file format of Calligra is the Open Document Format, ODF. Compatibility with other suites and formats has always been a priority and as usual all the import filters for Microsoft formats have received attention and improvements. At this time, the Calligra import filters for docx/xlsx/pptx are the best free translations tools available anywhere.

## Other Than the Desktop

At the same time as the desktop version, the community also release two other interfaces: Calligra Mobile and Calligra Active.

**Calligra Mobile** is the classic UI for mobile phones. The most well-known platform for this one is the Nokia N900. There are no new features for Calligra Mobile in this release.

**Calligra Active** is a newer and flashier document viewer for touch based tablets especially tailored for the Plasma Active platform. Calligra Active has a new search easy-to-use interface for spreadsheets and presentations.

## Try It Out

The source code of the snapshot is available for download: [calligra-2.3.81.tar.bz2](http://download.kde.org/download.php?url=unstable/calligra-2.3.81/calligra-2.3.81.tar.bz2)

### Ubuntu

Users of Ubuntu and Kubuntu are urged to try the daily snapshots prepared by [Project Neon](https://launchpad.net/project-neon). Paste the following in a terminal window and you'll find Calligra installed in /opt:

sudo add-apt-repository ppa:neon/ppa \\
&& sudo apt-get update\\
&& sudo apt-get install project-neon-base \\
   project-neon-calligra \\
   project-neon-calligra-dbg

You can run these packages by adding /opt/project-neon/bin to your PATH.

### Arch Linux

Arch Linux provides Calligra packages in the \[kde-unstable\] repository.

### Fedora

Fedora is working on packages and expects them to be available within the next two weeks.

### OpenSuse

There are OpenSUSE Calligra packages in the [unstable playground repository](https://build.opensuse.org/project/show?project=KDE%3AUnstable%3APlayground).

### FreeBSD

Calligra ports are available in [Area51](http://freebsd.kde.org/area51.php).

### Windows and OSX

Work has started on a Windows package of the Calligra Application Suite but we are not done yet, so for a Windows build, you will have to wait. We would welcome volunteers who want to build the Calligra Suite on OSX.

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra-suite-org/](http://www.calligra-suite.org/).

 

 

The project management application
