---
title: Calligra 2.9 Released
date: 2015-02-26
author: jstaniek
categories: []
---

We are happy to announce the release of final version 2.9 of the [Calligra Suite](http://www.calligra.org "Home page of the Calligra Suite"), Calligra Active and the Calligra Office Engine. This version is the result of thousands of changes which provide new features, polishing of the user experience and bug fixes.

[Support Calligra!](https://www.calligra.org/news/calligra-2-9-released/#support)

## What's in the Box?

[![ImagineFX Artist Choice Award](/assets/img/krita-artist-award.png)](https://krita.org/item/krita-receives-artist-choice-award-from-imaginefx/)**The 2.9 release is so far the biggest release for Krita**, the award-winning free and open source digital painting application. Eleven out of twelve of the features were [requested by users](https://krita.org/item/the-votes-are-in/) and funded by [Krita's first Kickstarter action](https://www.kickstarter.com/projects/krita/krita-open-source-digital-painting-accelerate-deve) and the twelfth feature will come in 2.9.1!

- Support for **loading and showing multiple images in one window**, and viewing any given image in multiple views and windows.
- **Fully integrated the G'Mic set of image manipulation tools**, enabling artists to, for instance, [greatly speed-up their workflow](http://www.davidrevoy.com/article247/krita-2-9-comic-coloring-tutorial).
- Greatly extended support for **painting in HDR mode**, making it a truly creative tool.
- **New perspective painting assistants to new color selectors**, **improved transform tools** and **non-destructive transformation masks**, **brush engine** improvements, workflow improvements, new filters, support for creating and installing resource packs (brushes, gradients, patterns) and many more.
- More details on [krita.org](https://krita.org/item/krita-2-9-0-the-kickstarter-release/)

\[caption id="attachment\_4513" align="aligncenter" width="300"\][![Professional artwork by David Revoy made with Krita (http://www.peppercarrot.com)](/assets/img/krita_2.9_texture_sm.png)](http://www.calligra.org/wp-content/uploads/2015/02/krita_2.9_texture.jpg)Professional artwork by David Revoy made with Krita ([http://www.peppercarrot.com](http://www.peppercarrot.com))\[/caption\]  

**The debut of Calligra Gemini, a novel mix of a traditional desktop app and a touch-friendly tablet app.**  
It encases Calligra's word processor and presentation apps. ([details](https://dot.kde.org/2014/11/21/calligra-gemini-added-calligra-suite))

[![Text document edited on laptop computer](/assets/img/calligra-gemini-desktop-2.9-sm.png)](http://www.calligra.org/wp-content/uploads/2014/12/calligra-gemini-desktop-2.9.png)[![The same text document in tablet mode](/assets/img/calligra-gemini-touch-2.9-sm-300x200.png)](http://www.calligra.org/wp-content/uploads/2014/12/calligra-gemini-touch-2.9.png)

The same text document edited on laptop computer and in tablet mode

  

**Kexi, visual data-oriented apps builder received over 150 improvements** that make it extra stable and easy to use.

- **Newer technologies have been employed** for the tabular (data grid) views and forms.
- Report Designer, Query Designer and data import assistants have improved substantially. ([details](https://community.kde.org/Kexi/Releases/Kexi_2.9#Changes_in_2.9.0))
- All that is spiced with **a dedicated [support](http://kexi-project.org/pics/2.9/kexi-2.9-breeze-icons.png "Dedicated support for Breeze icon theme of Plasma 5 in Kexi 2.9") for KDE Plasma 5's look and feel**.
- At the organizational level **Kexi gained a corporate partner, Milo Solutions** ([details](http://milosolutions.com/blog/dont-be-afraid-foss-is-here/))  
    The 2.9 release already contains contributions from Milo software engineer, Roman Shtemberko. ([read Roman's experience](http://milosolutions.com/blog/my-foss-experience/))

\[caption id="attachment\_4391" align="aligncenter" width="300"\][![New table view in Kexi 2.9](/assets/img/kexi-2.9-tableview_sm.png)](http://www.calligra.org/wp-content/uploads/2015/01/kexi-2.9-tableview.png) New table view in Kexi 2.9\[/caption\]  

**Unmatched integration: Displaying office documents in [Okular](https://okular.kde.org)**, KDE's universal document viewer. For displaying many types of documents _Calligra Office Engine_ has been used, the same that forms a pillar of document viewers on [Nokia N9](https://www.calligra.org/news/calligra-provides-the-engine-for-nokias-harmattan-office/) and [Jolla](http://jolla.com/jolla) smartphones, [COffice](https://play.google.com/store/apps/details?id=org.kde.calligra.coffice) Android app and more. ([details](/news/calligra-2-9-beta-released/#whats_new_integration))

\[caption id="attachment\_4325" align="aligncenter" width="300"\][![Calligra document plugin for Okular](/assets/img/okular-generator-2.9-300x200.png)](http://www.calligra.org/wp-content/uploads/2014/12/okular-generator-2.9.png) Calligra document plugin for Okular  
showing a DOC file\[/caption\]  

**Dozens of general improvements in common Calligra features as well as Calligra Sheets, Words** are present in the 2.9 series. For details jump to the [Beta 1](https://www.calligra.org/news/calligra-2-9-beta-released/), [Beta 2](https://www.calligra.org/news/calligra-2-9-beta-2-released/) and [Beta 3](https://www.calligra.org/news/calligra-2-9-beta-3-released/) change logs.  
  

## Try It Out

![Download small](/assets/img/download-70.png)

The source code of the release is available for download here: [calligra-2.9.0.tar.xz](http://download.kde.org/stable/calligra-2.9.0/calligra-2.9.0.tar.xz). Also [translations to many languages](http://download.kde.org/stable/calligra-2.9.0/calligra-l10n/) and [MD5 sums](http://download.kde.org/stable/calligra-2.9.0/MD5SUMS). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Stable_Release).

  

## What's Next and How to Help?

We have approached the era of [2.9](https://community.kde.org/Calligra/Schedules/2.9/Release_Plan). The next step, Calligra 3.0, will be based on new technologies. Expect it later in 2015.

You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated to [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita). Many improvements are only possible thanks to the fact that we're working together within the awesome community.

(Some Calligra apps need new [maintainers](https://community.kde.org/Calligra/Maintainers), you can become one, it's fun!)

## How and Why to Support Calligra?

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, traveling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to: ![Heart](/assets/img/heart-701.png)

**Support entire Calligra indirectly by donating to KDE**, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations).

![Heart](/assets/img/heart-701.png)

**Support Krita directly by donating to the Krita Foundation**, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/).

![Heart](/assets/img/heart-701.png)

**Support Kexi directly by donating to its current BountySource fundraiser**, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

  

## About the Calligra Suite

![](/assets/img/calligra-logo-transparent-for-light-600-300x200.png)

Calligra Suite is a graphic art and office suite developed by the KDE community. It is available for desktop PCs, tablet computers and smartphones. It contains applications for word processing, spreadsheets, presentation, databases, vector graphics and digital painting. For more information visit [calligra.org](http://www.calligra.org/).

  

## About KDE

![](/assets/img/klogo-official-lineart_simple_bw-128x128.png)

[KDE is an international technology team](https://www.kde.org/community/whatiskde/) that creates free and open source software for desktop and portable computing. Among KDE's products are a modern desktop system for Linux and UNIX platforms, comprehensive office productivity and groupware suites and hundreds of software titles in many categories including Internet, multimedia, entertainment, education, graphics and software development. KDE's software available in more than 60 languages on Linux, BSD, Solaris, Windows and Mac OS X.

<!-- h3 { padding-top:1em; } .button, .button:link, .button:visited { text-decoration:none; text-align:center; padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:hover{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } .button:active{ padding:11px 32px; border:solid 1px #004F72; -webkit-border-radius:10px; -moz-border-radius:10px; border-radius: 10px; font:18px Arial, Helvetica, sans-serif; font-weight:bold; color:#E5FFFF; background-color:#3BA4C7; background-image: -moz-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -webkit-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -o-linear-gradient(top, #3BA4C7 0%, #1982A5 100%); background-image: -ms-linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1982A5', endColorstr='#1982A5',GradientType=0 ); background-image: linear-gradient(top, #3BA4C7 0% ,#1982A5 100%); -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; -moz-box-shadow: 0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff; } <div></div> .button a,.button a:link, .button a:visited, .button a:hover, .button a:active { color:#E5FFFF; } -->
