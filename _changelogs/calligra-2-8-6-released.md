---
title: Calligra 2.8.6 Released
date: 2014-09-24
author: jstaniek
categories: []
---

We are pleased to announce that [Calligra Suite, and Calligra Active](http://www.calligra.org) 2.8.6 have just been released. This last recommended update brings over 60 improvements to the 2.8 series of the applications and underlying development frameworks.

We are also glad to note that this release contains first contributions from Kamil Łysik, Michał Poteralski, and further improvements from the new contributor Wojciech Kosowicz. All for Kexi. Welcome on board!

## What's Next and How to Help?

Now we're focusing on 2.9 series planned to debut in December this year and the new shiny 3.0 for 2015. You can meet us to share your thoughts or offer your support on [general Calligra forums](http://forum.kde.org/calligra) or dedicated [Kexi](https://forum.kde.org/kexi) or [Krita](https://forum.kde.org/krita) forums. Many improvements are only possible thanks to the fact that we're working together within the awesome community.

**(if you'd like to maintain entire app, [Karbon](https://www.calligra.org/karbon/) and [Plan](https://www.calligra.org/plan/) need new [maintainers](https://community.kde.org/Calligra/Maintainers))**

Calligra apps may be totally free, but their development is costly. Power, hardware, office space, internet access, traveling for meetings - everything costs. Direct donation is the easiest and fastest way to efficiently support your favourite applications. Everyone, regardless of any degree of involvement can do so. You can choose to:

- Support entire Calligra indirectly by donating to KDE, the parent organization and community of Calligra: [http://www.kde.org/community/donations](http://www.kde.org/community/donations),
- Support Krita directly by donating to the Krita Foundation, to support Krita development in general or development of a specific feature: [https://krita.org/support-us/donations](https://krita.org/support-us/donations/),
- Support Kexi directly by donating to its current BountySource fundraiser, supporting development of a specific feature, or the team in general: [https://www.bountysource.com/teams/kexi](https://www.bountysource.com/teams/kexi).

## Issues Fixed in This Release

### General

- Make rulers accept minimum length equal to zero. (bug 334968)
- Fix a crash while copy/pasting (bug 334832)
- Set the docker's small font also on tool option panels for consistency.
- Use the list of available file types when setting the file filter for picture shape. (bug 327782)
- Put the New view action back to the View menu.
- Prevent backtracking to undo the layout of a whole page, thus starting an infinite loop. This can be triggered by a page break in the middle of keepWithNext paragraphs. (bug 306000)

### Kexi

- Fix logic behind visibility of editing indicator in tabular and form views
- Make Next and Last button in Page selector of Report data view to be disabled on last page.
- Fix build error on OpenBSD operating system (missing include directory for ICU).
- Fix row selection/highlighting when clicking/hovering over record marker. (bug 337914)
- Fix an issue with resolving version of plugins (at least) on Linux.
- Improve behaviour of tabbed toolbar when Design Tab is present.
- Avoid displaying actions for parts that do not create objects.
- Set labels to right in the Find dialog.
- Fix a crash when pressed Tab on a form with one date picker. (bug 338766)
- Avoid crashes when closing Kexi views.
- Allow to create report items of specific size using mouse drag. (bug 334967)
- Fix crash in Report designer.
- Fix possible crash on opening report with a barcode element.
- Fix icon alignment in Kexi record navigator buttons.
- Add running desktop name and version to the feedback agent. Recognizes Plasma 4 and 5 at least.
- Position pasted report element with an offset to the active element or to the corner. (bug 334967)
- Add donations info to Kexi's status bar. Currently only opens a BountySource page.
- Improve experience by adding a dedicated database password dialog.
- Avoid keeping potentially wrong password entered in the password dialog. We don't want to re-use it.
- Improve default size of modal assistant dialogs.
- Hide system PostgreSQL (postgres) and MySQL (performance\_schema) databases from the visible database list.
- Use consistent sorting of unicode text values. (bug 338808)
- Fix background opacity handling and new background/foreground's defaults for report elements.
- Many fixes for Kexi's Database Import assistant (bugs 336556, 336557, 336558):
    - When needed ask for password to access source or destination servers.
    - Reload source database list if user pressed back and selected different connection.
    - Improve saving recent directories.
    - Do not ask twice about destination filename.
    - In addition to title, properly ask for database name for server destination databases.
    - Properly open imported database (file).

### Krita

- Fix bug that randomly disables tablet support.
- Fix a crash with the latest _evdev_ tablet drier.
- Fix recognition of the Wacom stylus' serial ID.
- Add an option to disable touch capabilities of Wacom tablets. If you want to use it, please add the following option to your kritarc configuration file: disableTouchOnCanvas=true. (bug 332773)
- Make rotation on Linux be consistent with rotation on Windows. (bug 331358)
- Fix tablet stylus rotation on Linux.
- Improve performance of the OpenGL canvas, it's twice faster now.
- Reduce memory consumption when textures are updated.
- Fix a crash in Channels docker when the image is closed.
- Fix a crash when the number of patches that fits in a column is zero.
- Use period key instead of non-standard stop key for the "Select previous favourite preset" command. (bug 331105)
- Fix moving of shapes in groups in shape layers. (bug 308942)
- Set focus to layer name text input in layer properties dialog.
- Fix the HSV options bug. (bug 313494)
- Round the corner point position of the rectangle base tool, otherwise there is some odd jumping. (bug 335144)
- Fix Erase composite operation to handle the selections properly.
- Fix the Blur filter when Y>X. (bug 336914)
- Fix a crash after selecting Layer array clones more than a couple times. (bug 336804)
- Don't crash when trying to Ctrl-Alt pick a color from a group layer.
- Don't show icons in the menus on Windows.
- Fix anisotropy in Color Smudge brush engine (offset calculation). (bug 334538)
- Fix Lens Blur filter artifacts when used as an Adjustment Layer. (bug 336470)
- Fix a hang-up when opening the filter dialog twice or running any stroke-based action while having the dialog open. (bug 334982)
- Fix misbehavior of the Brush tool when selected on a vector layer. (bug 335660)
- Fix saving 16 bit grayscale images to tiff, jpeg and ppm. (bug 338478)

### Calligra Stage

- Let Ctrl+Home and Ctrl+End keys go to top and bottom of text shape in stage.
- Allow to exit text-edit mode by pressing Escape.

### Try It Out

- The source code is available for download: [calligra-2.8.6.tar.xz](http://download.kde.org/stable/calligra-2.8.6/calligra-2.8.6.tar.xz).
- Calligra binaries are available for [download](http://userbase.kde.org/Calligra/Download) for many operating systems.

## About Calligra

Calligra Suite is a graphic art and office suite. It is a part of the applications family from the KDE community. See more information at the website [http://www.calligra.org](http://www.calligra.org/).
