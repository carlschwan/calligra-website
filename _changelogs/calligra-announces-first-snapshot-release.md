---
title: Calligra Announces First Snapshot Release
date: 2011-05-18
author: Inge Wallin
categories: []
---

Today Calligra project announces the first snapshot release of the Calligra suite.  It is now 5 months ago since Calligra and KOffice split ways and during that time we have improved our core libraries as well as improved all the applications themselves.

Our goal is to provide the best application suite on all platforms based on open standards.  That is no small goal.  We feel now that we have improved the foundation enough to start working seriously on improving the user experience.  For this, we need the help of our users!

We will therefore release monthly snapshots, starting today, until the final release in October.  These snapshots will be packaged by most major Linux distributions and  instructions on how to install them will be provided too.  We hope to get lots of feedback  on the snapshots so that in October we can finally announce "Calligra is now end user ready and is the most usable free office suite of all".

## For Users

So what has happened since the formation of the Calligra project? Development has speeded up a lot since the break.  We have several new contributors, and some old contributors have come back into the project. Here are some of the results that make us proud to announce this first snapshot in the upcoming series:

### Applications

We have added two new applications:

- **Flow** is a diagramming and flowcharting application in the spirit of Visio.
- **Braindump** is a note taking application that builds on the Calligra core and uses the full power of the openDocument Format to allow for text, images and even multimedia in the notes.

### User Interface Work

We have worked to identify the weaknesses in the user interfaces that we have.  Help in this have been user interface design people, both from within the community and from outside it. We have also started to work on implementing the ideas that have come up, but this is far from finished yet.

We urge our testers to be patient in this area, since in this snapshot the user interface is still far from good. We are aware of that. The next snapshot will show the first improvements in the user interface, but we are interested in feedback already for this one.

### Compatibility:

All our file conversion plugins have been improved a lot. Among the new features is support for Smart Art in MS Office 2007 files and later. Please test your MS documents and report any errors.

### Improved Performance

The core has been significantly speeded up and in some cases been made more lightweight. This will improve the ability for Calligra to work in limited environments like mobile phones and older computers.

### Improved Text Layout

The text layout and rendering library has been completely reworked. It is now possible to render tables with page breaks, tables within tables, to have editable footnotes and endnotes and to get rendering of headers and footers correct. None of this was possible with the old text layout code.

At the same time we also added basic support for sections as defined in ODF and the new text layout makes it much easier to later implement full section support with a multi-column layout. The text layout library is used in all parts of Calligra.

### Calligra on Non-desktop Platforms

Calligra is available on tablets and smartphones. The user interfaces are called Calligra Active, and Calligra Mobile, respectively. Calligra Active is part of the Plasma Active initiative, and runs or will soon run on MeeGo. Calligra Mobile was previously known as FreOffice and runs on the Nokia N900 smartphone. It can also be packaged for other smartphones, but we are not aware of any other actual packages.

Both Calligra Active and Calligra Mobile are distributed with the source code.

## For Developers and System Integrators

The Calligra Suite is uniquely configurable and embeddable. We have made the decision to not only support the desktop, but also other environments like tablets and smartphones. We are now starting to see the benefits of the work of the last 5 months here.

### Improved Separaration of Engine and UI

We have improved the separation of the core of Calligra -- the Office Engine -- from the user interfaces.  This means that it is now even easier than before to create new user interfaces for new situations.

In our repository we have two working user experiences: the desktop one and one aimed at mobile platforms, Calligra Mobile. There is also a new mobile user experience based on Qt Quick being developed. We have great hopes on this one since it will allow the user interface to adapt easier to new form factors like tablets.

In addition to these community based ones, we know of at least 3 company sponsored user interfaces that are not yet released.

## Towards the Future

Calligra is much more lightweight than other free office suites, it has a good separation between engine and user interface and there are already several mobile user interfaces.  We therefore expect Calligra to be the dominant free office application on mobile platforms.

We also have a good foundation to stand on.  What is not yet fully mature is the desktop interface.  The developer team discussed that a lot at the Calligra developer sprint in Berlin, and we have a plan to improve and simplify the user interface for the desktop.

We will work on improving this until the final release, and that is what we need our users to participate in.  So install the Calligra snapshot and test it out.  Report to us bugs and bad usability and together we will make Calligra not only the best mobile application suite but also the best desktop application suite!

## Trying Out the Calligra Snapshot

Many Linux distributions will package the snapshots for users to try out. Instructions will be available on the home pages of the distributions. We will also add links to them on [our home page](http://www.calligra-suite.org/).

If you want to build the source code, you can find it at [the kde download servers](http://download.kde.org/download.php?url=unstable/calligra-2.3.71/calligra-2.3.71.tar.bz2).

For those that want to know beforehand what they are getting into, we have prepared [a visual tour](http://www.calligra-suite.org/uncategorized/calligra-2-4-snapshot-1-tour/) that shows off some parts of the Calligra Suite.
