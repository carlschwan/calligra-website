---
title: Calligra 2.4.1 Released
date: 2012-04-25
author: Inge Wallin
categories: []
---

The Calligra team has released version 2.4.1, the first bugfix release of the [Calligra Suite](http://www.calligra.org/ "Home page of the Calligra Suite"), Calligra Mobile and Calligra Active. This release contains a number of important bug fixes to 2.4.0 and we recommend everybody to update as soon as possible.

## Bugfixes in This Release

The following fixes are in 2.4.1:

###  General:

- Fix: saving to ODT when compiling with gcc 4.7 BUG: 298134
    
- Remove the filter colors functionality in in the color popup BUG:288071
    
- Compilation fixes for gcc 4.7 and Solaris.
    
- Fix: updating of the screen with the rectangle tool
    
- Increase maximum zoom level to 6400%
    

### Words:

- Fix: the updating of styles  BUG: 296487
    
- Fix layout and rendering bug in table borders
    
- Fix crash during load of doc with custum numbered pages BUG:298106
    
- Fix crash if the statistics docker is counting  while closing a document BUG:298172
    

### Sheets:

- Fix crash when saving a document that does not have styles
    
- Fix saving of the shapes offset when they are cell-anchored
    

### Kexi:

- Fix: Cannot update field name from Property Editor BUG:278296
    
- SQLite: Fixed error when attempting to compact db opened from Open view
    

### Karbon:

- Fix: crash in loading svg file from blender
    

### Krita:

- Recognise image types based on content rather than extension BUG:297656
    
- Fix order of channels in the specific color selector BUG:297715
    
- Fix many dockers that where forcing tabbed dockers to use the smallest possible size. BUG:297537
    
- Disable the color dialog popup for the foreground/background button
    
- Fix position of the text shape inserted by krita's text tool
    
- Disable the tab and lock icon for Krita's tool option widgets
    
- Make histogram calculation faster
    

## Try It Out

The source code of this version is available for download here: [calligra-2.4.1.tar.bz2](http://download.kde.org/stable/calligra-2.4.1/calligra-2.4.1.tar.bz2). Also many [binary packages](http://userbase.kde.org/Calligra/Download) are available for various operating systems and distributions. This release is packaged for Windows by [KO GmbH,](http://www.kogmbh.com/ "KO GmbH homepage") and you can find it on their [download page](http://www.kogmbh.com/download.html).

We would welcome volunteers who want to build the Calligra Suite on OSX.

## About Calligra

Calligra is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
