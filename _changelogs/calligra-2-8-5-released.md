---
title: Calligra 2.8.5 Released
date: 2014-07-05
author: jstaniek
categories: []
---

This is the [last but one](http://community.kde.org/Calligra/Schedules/2.8/Release_Plan) update to the 2.8 series of the [Calligra Suite, and Calligra Active](http://www.calligra.org) released to fix recently found issues. The Calligra team recommends everybody to update.

Why is 2.8.4 skipped? Shortly before 2.8.4 release we discovered bug that sneaked in 2.8.2 version and decided to skip the 2.8.4 entirely and quickly release 2.8.5 instead with a proper fix. The bug is related to [not showing file formats in Save dialogs](http://lists.kde.org/?l=calligra-devel&m=140386344927957&w=2).

## Issues Fixed in This Release

### General

- Show file formats in Save dialogs
- Added a number of missing translations

### Kexi

- Only move to initial top-left position in table view on initial show (bug 334577)
- Display proper record's page number when coming back to report's data view (bug 335269)
- Properly remember previous search keywords in the Find Dialog (bug 334514)
- Fix crash for SELECT queries containing (invalid) "ORDER BY 0" parameter
- Properly sort names of newly added tables and queries in Form Designer's data source drop-down list (bug 334093)
- Fix crash possible when accessing queries with duplicated table names. Example query "SELECT t.foo FROM t, t". Display error message so user can fix the statement. (bug 315852)
- Fix squashed up toolbar buttons for windows-based styles (bug 335149)
- Fix crash when removing a Database Connection (bug 315288)
- Temporarily hide "sort" buttons in forms because sorting is not implemented there anyway (related to bug 150372)
- Make Ctrl+S keyboard shortcut work for Save action in all designers (bug 334587)
- Fix deselecting text form's text editor widget when moving to other widget (bug 336054)
- Let the spreadsheet import plugin to be found by Kexi
- Fix crash on changing Auto Tab Order form property, set to true by default (bug 336927)
- Properly retrieve map element after saving and re-opening report (bug 336985)
- Removed resource leaks

### Calligra Stage

- Re-enable KPresenter (KPR)-to-Open Document Presentation (ODP) document filter

### Try It Out

- **The source code** is available for download: [calligra-2.8.5.tar.xz](http://download.kde.org/stable/calligra-2.8.5/calligra-2.8.5.tar.xz).
- Calligra binaries are available for [download](http://userbase.kde.org/Calligra/Download) for many operating systems.

## About Calligra

Calligra Suite is a graphic art and office suite. It is a part of the applications family from the KDE community. See more information at the website [http://www.calligra.org](http://www.calligra.org/).
