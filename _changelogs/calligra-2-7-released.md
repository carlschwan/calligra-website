---
title: Calligra 2.7 Released
date: 2013-08-01
author: Inge Wallin
categories: []
---

The Calligra team is proud and pleased to announce the release of version 2.7 of the [Calligra Suite](http://www.calligra-suite.org/ "Home page of the Calligra Suite"), Calligra active and the Calligra Office Engine. This version is the result of thousands of commits which provide new features, polishing of the user interaction and bug fixes.

The version number of this release is actually 2.7.1, not 2.7.0 because a serious problem was found after the tagging of 2.7.0. Due to the release process, the team had to withdraw 2.7.0 and instead release 2.7.1 as the first stable version of the 2.7 series.

## News in This Release

Calligra 2.7 contains a number of new features. Here is an excerpt of the most important ones.

**Words,** the word processing application, has a **new look for the toolbox**. In the same toolbox there are also **new controls to manipulate shapes** with much enhanced usability.  **Styles are now sorted with the used ones first** in the style selector which should make using styles much more convenient. There are also enhancements to the **reference controls**, especially regarding hyperlinks and bookmarks.

**Author,** the writer's application, has new **support for EPUB3**: mathematical formulas and multimedia contents are now exported to ebooks using the EPUB format.  There is also new support for **book covers using images.**

\[caption id="attachment\_3919" align="aligncenter" width="277"\][![words-sidebar](/assets/img/words-sidebar-277x400.png)](http://www.calligra.org/wp-content/uploads/2013/07/words-sidebar.png) The new toolbox for Words, Author, Sheets and Stage inside the sidebar.\[/caption\]

There is a **new export filter for common text** and **enhancements to the docx import filter**, which can now support predefined table styles. Note that all of these enhancements to Words and Author are also available in the other application. We note here where they were originally developed.

**Sheets,** the spreadsheet application, now uses the same **new toolbox** as Calligra Words.

**Plan,** the project management application, has **improvement in the scheduling of tasks** and **new export filters** to OpenDocument Spreadsheet (ODS) and CSV.

**Kexi**, the visual database creator, improves **CSV data import**, in particular now it can import data into an existing table. Passwords do not require modal dialogs and are generally better handled. See the [full list of changes](http://community.kde.org/Kexi/Releases/Kexi_2.7).

**Shapes:** There are some improvements in the general shapes available from most Calligra applications: The formula shape now has new ways to enter formula: **a matlab/octave mode** and a **LaTEX mode**. The tool for the text shape is improved to better handle text styles.

Another change common to Words, Author and Stage is an **improved visual style editor**.

\[caption id="attachment\_3921" align="aligncenter" width="383"\][![shape-controls](/assets/img/shape-controls-383x400.png)](http://www.calligra.org/wp-content/uploads/2013/08/shape-controls.png) The new shape editor, also known as shape control.\[/caption\]

**Krita,** the 2D paint application, has **new grayscale masks**, a **better freehand path tool** with line smoothing, a **way to paint shapes with Krita brushes**, **improved color to alpha filter**, an **improved crop tool** and an **improved transform tool** that helps the user create textures. **Support for new file formats** include export to **QML** and a much **improved import/export filter for photoshop PSD files**. You can read about the improvements in Krita in more details on [krita.org](http://krita.org/).

 

## Try It Out

The source code of the snapshot is available for download: [calligra-2.7.1.tar.xz](http://download.kde.org/stable/calligra-2.7.1/calligra-2.7.1.tar.xz). Alternatively, you can [download binaries for many Linux distributions and for Windows](http://userbase.kde.org/Calligra/Download#Unstable_Release).

## About the Calligra Suite

The Calligra Suite is part of the applications from the KDE community. See more information at the website [http://www.calligra.org/](http://www.calligra.org/).
